class ComplexNumber{

    _real: number;
    _imaginary: number;

	constructor(real:number,imaginary:number){
		this._real=real;
		this._imaginary=imaginary;
	}

	get real(){
		return this._real
	}

	get imaginary(){
		return this._imaginary;
	}

	get r(){
		return Math.sqrt(this.magnitude());
	}

	get theta(){
		return Math.atan(this._imaginary/this._real);
	}

	toString(){
		return `(${this._real}+${this._imaginary}i)`;
	}

	add(o:ComplexNumber|number):ComplexNumber {
		if(o instanceof ComplexNumber)
			return new ComplexNumber(this._real + o._real, this._imaginary + o.imaginary);
		return this.add(new ComplexNumber(o,0));
	}

	sub(o:ComplexNumber|number):ComplexNumber {
		if(o instanceof ComplexNumber)
			return new ComplexNumber(this._real - o._real, this._imaginary - o.imaginary);
		return this.sub(new ComplexNumber(o,0));
	}

	neg() {
		return this.muli(ComplexNumber.NONE);
	}

	muli(o:ComplexNumber|number):ComplexNumber{
		if(o instanceof ComplexNumber)
			return new ComplexNumber(this._real * o._real-this._imaginary * o.imaginary, o._real * this._imaginary+this._real * o.imaginary);
		return this.muli(new ComplexNumber(o,0));
    }
    
    mult(o:ComplexNumber|number):ComplexNumber{
        return this.muli(o);
    }
	//I know that this is not mathematically correct. Will fix it at some point
	sin(){
		return new ComplexNumber(Math.sin(this._real),Math.sin(this._imaginary));
	}

	exp(){
		return new ComplexNumber(Math.exp(this._real)+Math.cos(this._imaginary),Math.sin(this._imaginary));
	}

	inv(){
		return new ComplexNumber(this._real,-this._imaginary);
	}

	isFinite() {
		return Number.isFinite(this._real) && Number.isFinite(this._imaginary);
	}

	divise(o:ComplexNumber|number):ComplexNumber{
		if(o==0)
			throw new Error("x/0 is not allowed");
		if(o instanceof ComplexNumber){
			let val=(o._real*o._real+o.imaginary*o.imaginary);
			return new ComplexNumber((this._real*o._real+this._imaginary*o.imaginary)/val,(this._imaginary*o._real-this._real*o.imaginary)/val);
		}
		return this.divise(ComplexNumber.from(o));
	}

	pow(o:ComplexNumber|number):ComplexNumber {
		if(o instanceof ComplexNumber)
            return this.powComplex(o);
        if(o<=0)
            return ComplexNumber.ONE;
        return this.muli(this.pow(o-1));
	}

	powComplex(o:ComplexNumber):ComplexNumber{
		let logpart=Math.log(this.r);
		return (new ComplexNumber(logpart*o._real,logpart*o._imaginary).add(o.muli(new ComplexNumber(0,this.theta)))).exp();
	}

	negInfinity() {
		let f=Number.NEGATIVE_INFINITY;
		return f==this._real||f==this._imaginary;
	}

	magnitude(){
		return this._real**2+this._imaginary**2;
    }
    
    static from(num:number){
        return new ComplexNumber(num,0);
    };
    static ZERO = ComplexNumber.from(0);
    static ONE = ComplexNumber.from(1);
    static NONE = ComplexNumber.from(-1);
}
