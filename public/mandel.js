var CANVSIZE=800;
let lowerX=lowerY=-2.0;
let higherX=higherY=2.0;
const coloring={
  "Strict":(n)=>`rgb(${n%255},${n%255},${n%255})`,
  "Wild":(n)=>`rgb(${Math.sin(n)*255},${Math.tan(n)*255},${Math.cos(n)*255})`,
  "Sin":(n)=>{n=n/(2*Math.PI);let nr=Math.sin(n)*255;return "rgb("+nr+","+nr+","+nr+")";}
  ,"Tan":(n)=>{n=n/(2*Math.PI);let nr=Math.tan(n)*255;return "rgb("+nr+","+nr+","+nr+")";}
  ,"Cos":(n)=>{n=n/(2*Math.PI);let nr=Math.cos(n)*255;return "rgb("+nr+","+nr+","+nr+")";}
  ,"Square":(n)=>{let nr=Math.pow(n,2)%255;return "rgb("+nr+","+nr+","+nr+")";}
  ,"Log":(n)=>{let nr=(Math.log(n)*200)%255;return "rgb("+nr+","+nr+","+nr+")";;}
  ,"Log2":(n)=>{let nr=(Math.log2(n)*200)%255;return "rgb("+nr+","+nr+","+nr+")";;}
  ,"Red Spectrum":(n)=>{return "rgb("+Math.sin(n)*255+","+0+","+0+")";}
  ,"Stars method":(n,small)=>{let nr=(small.magnitude()*255)%255; return "rgb("+nr+","+n%255+","+n%255+")"}
  ,"Enhanced Stars method":(n,small)=>`rgb(${(Math.abs(small.real)*255)%255},${(Math.abs(small.imaginary)*255)%255},${n%255})`
  ,"Broad":(n,x,small)=>{return "rgb("+(Math.abs(small.real)*255)%255+","+(Math.abs(small.imaginary)*255)%255+","+n%255+")"}
  ,"Quadrant":(n,_1,_2,fin)=>{let nr=Math.tan(fin.imaginary/fin.real)*255;return "rgb("+nr+","+0+","+0+")";}
  ,"Smooth":(n,_1,_2,fin)=>{let smoothVal=n - (Math.log2(Math.abs(Math.log2(fin.magnitude()))) - Math.log2(Math.log2(255)))/Math.log2(2);return "rgb("+smoothVal*255+","+n%255+","+Math.sin(n)*255+")"}
  ,"Vand":(n,small,oSmall,fin)=>{return "rgb("+((Math.log2(n))*200)%255+","+((Math.sin(small.magnitude()-oSmall.magnitude())+Math.cos(small.magnitude()-oSmall.magnitude())*Math.tan(oSmall.magnitude()/fin.magnitude()))*255)%255+","+(Math.abs(small.real/fin.real)*255)%255+")"}
  ,"Corals":(n,small,oSmall,fin)=>{return "rgb("+((Math.log2(n))*200)%255+","+((Math.sin(small.magnitude()-oSmall.magnitude())*Math.tan(fin.magnitude()))*255)%255+","+(Math.abs(small.real/fin.real)*255)%255+")"}
  ,"Streams":(n,small,oSmall,fin)=>{return "rgb("+((Math.log2(n))*200)%255+","+((Math.sin(small.magnitude()-oSmall.magnitude())+Math.cos(small.magnitude()-oSmall.magnitude())*Math.tan(fin.magnitude()))*255)%255+","+(Math.abs(small.real/fin.real)*255)%255+")"}
  ,"Sparkles":(n,small,oSmall,fin)=>{return "rgb("+((Math.log2(n))*200)%255+","+((Math.sin(small.magnitude()-oSmall.magnitude())+Math.cos(small.magnitude()-oSmall.magnitude())*Math.tan(fin.magnitude()/oSmall.magnitude()))*255)%255+","+(Math.abs(small.real/fin.real)*255)%255+")"}
}
const functions={
  "Mandelbrot":{
    "isReady":false,
    "construct":function(compl){return (x)=>{return x.pow(2).add(compl);}; },
    "use":ComplexNumber.ZERO
  },
  "Mandelbrot K":{
    "isReady":false,
    "construct":function(compl,k){return (x)=>{return x.pow(k).add(compl);};},
    "use":ComplexNumber.Zero
  },
  "Egu":{
    "isReady":true,
    "function":(x)=>{return x.pow(2).add(ComplexNumber.ONE.muli(0.3));},
    "use":null
  },
  "Trues K":{
    "isReady":false,
    "construct":function(_,k){return (x)=>{return x.pow(k).add(ComplexNumber.ONE.muli(0.3));}}
  },
  "Feathers":{
    "isReady":false,
    "construct":function(compl){return (x)=>{return x.neg().muli(x.sin()).add(compl)}},
    "use":ComplexNumber.ZERO
  },
  "Closure K":{
    "isReady":false,
    "construct":function(compl,k){return (x)=>{return x.sin().add(compl).pow(k)}}
  },
  "Pearl K":{
    "isReady":false,
    "construct":function(compl,k){return (x)=>{return x.add(new ComplexNumber(Math.sin(k),0)).muli(compl).add(new ComplexNumber(compl.real+Math.cos(compl.imaginary),Math.tanh(compl.imaginary)));};},
    "use":ComplexNumber.ZERO
  },
  "Basins of attraction":{
    "isReady":true,
    "function":(x)=>{return x.sub(x.pow(3).sub(1).divise(x.muli(3).pow(2)))},
    "description":"This one takes literaly minutes. If you get anoyed, reload the page, but if you let it run, the outcome can look really cool"
  },
  "Boom":{
    "isReady":true,
    "function":(x)=>{return x.pow(4).add(new ComplexNumber(0.6,0.55));}
  },
  "Walls":{
  "isReady":false,
  "construct":function(comp,k){return (x)=>{return x.divise(new ComplexNumber(Math.sin(x.real+k),Math.pow(x.imaginary,k))).add(comp)};}
  }
}
const kernels = {
  "Motionblur":[[1, 0, 0, 0, 0, 0, 0],[0, 1, 0, 0, 0, 0, 0],    [0, 0, 1, 0, 0, 0, 0],    [0, 0, 0, 1, 0, 0, 0],    [0, 0, 0, 0, 1, 0, 0],    [0, 0, 0, 0, 0, 1, 0],    [0, 0, 0, 0, 0, 0, 1]],
  "Blur":[    [0, .2, 0],    [.2, .2, .2],    [0, .2, 0],  ],
  "Boxblur":[[1/9,1/9,1/9],[1/9,1/9,1/9],[1/9,1/9,1/9]],
  "Sharpen":[    [-1, -1, -1],    [-1, 9, -1],    [-1, -1, -1]  ],
  "Edges":[    [0, -1, 0],  [-1, 4, -1],   [0, -1, 0] ]
}

window.onload=function(){
  let canvas=document.createElement("canvas");
  canvas.width=canvas.height=CANVSIZE;
  canvas.con=canvas.getContext("2d");
  canvas.con.strokeStyle="black";
  canvas.drawingSurfaceImageData="";
  canvas.coloringMethod=coloring.Square;
  canvas.definingFunction=functions.Mandelbrot;
  canvas.K=3;
  canvas.touches={};
  canvas.touches.start=0;
  canvas.touches.stop=null;
  canvas.saveCurrent=function() {canvas.drawingSurfaceImageData = canvas.con.getImageData(0, 0, canvas.width,canvas.height);};
  canvas.restoreDrawingSurface=function() { canvas.con.putImageData(canvas.drawingSurfaceImageData, 0, 0);}
  canvas.ontouchstart=(evt)=>{evt.preventDefault();};
  canvas.ontouchmove=(evt)=>{
    evt.preventDefault();
    if(canvas.touches.start===0){
      canvas.touches.start=getMousePos(canvas,evt);		
    }else{
      let stop=getMousePos(canvas,evt);
      let start=canvas.touches.start;
          canvas.restoreDrawingSurface();
          let square={
            "start":{"x":Math.min(start.x,stop.x),"y":Math.min(start.y,stop.y)},
            "a":Math.max(Math.max(start.y,stop.y)-Math.min(start.y,stop.y),Math.max(start.x,stop.x)-Math.min(start.x,stop.x))
          };
          canvas.con.strokeRect(square.start.x,square.start.y,square.a,square.a);
      console.log("move");
    }
  };
  canvas.ontouchend=(evt)=>{
	  let start=canvas.touches.start;
	  canvas.touches.start=0;
	  let stop=getMousePos(canvas,evt)
	  let square={
        "start":{"x":Math.min(start.x,stop.x),"y":Math.min(start.y,stop.y)},
        "a":Math.max(Math.max(start.y,stop.y)-Math.min(start.y,stop.y),Math.max(start.x,stop.x)-Math.min(start.x,stop.x))
      };
      updateLHC(square);
      update(canvas);
	console.log("out");
	  evt.preventDefault();
  };
  canvas.onmousedown=function(evt){
    if(detectLeftButton(evt)){
      let start=getMousePos(canvas,evt);
      canvas.onmousemove=function(evtt){
        let stop=getMousePos(canvas,evtt);
        canvas.restoreDrawingSurface();
        let square={
          "start":{"x":Math.min(start.x,stop.x),"y":Math.min(start.y,stop.y)},
          "a":Math.max(Math.max(start.y,stop.y)-Math.min(start.y,stop.y),Math.max(start.x,stop.x)-Math.min(start.x,stop.x))
        };
        canvas.con.strokeRect(square.start.x,square.start.y,square.a,square.a);
		console.log("move");
		evtt.preventDefault();
      }
      canvas.onmouseup=function(evtt){ 
        let stop=getMousePos(canvas,evtt)
        canvas.onmousemove=null;
        let square={
          "start":{"x":Math.min(start.x,stop.x),"y":Math.min(start.y,stop.y)},
          "a":Math.max(Math.max(start.y,stop.y)-Math.min(start.y,stop.y),Math.max(start.x,stop.x)-Math.min(start.x,stop.x))
        };
        updateLHC(square);
        update(canvas);
		console.log("out");
		evtt.preventDefault();
      }
	  console.log("on");
	  evt.preventDefault();
    }
  }

  {
    let center=document.createElement("center");
    center.appendChild(canvas);
    let firstOtherThing=document.body.firstChild;
    document.body.insertBefore(center,firstOtherThing);
    let colorthyngy=colorMethodCheck(canvas);
    document.body.insertBefore(colorthyngy,firstOtherThing);
    let functionthyngy=functionCheck(canvas);
    document.body.insertBefore(functionthyngy,firstOtherThing);
    let convolutionthingy= convolutionCheck(canvas);
    document.body.insertBefore(convolutionthingy,firstOtherThing);
  }
  update(canvas);
}
function convolutionCheck(canv){
  let field = document.createElement("fieldset");
  field.id = "convo";
  {
    let legend = document.createElement("legend");
    legend.textContent="Convolutitons";
    field.appendChild(legend);
  }
  for(let kernel in kernels){
    let button = document.createElement("button");
    button.textContent = kernel;
    button.onclick=()=>{
      Filter(kernels[kernel])
        .canvas(canv);
    }
    field.appendChild(button);
  }
  return field;
}

function functionCheck(canv){
  let field=document.createElement("fieldset");
  field.id="func";
  {
    let legend=document.createElement("legend");
    legend.textContent="Function";
    field.appendChild(legend);
  }
  for(let func in functions){
    let span=document.createElement("span");
    span.textContent="Function: "+func;
    span.style="border:solid 2px black;white-space:nowrap;float:left;";
    let input=document.createElement("input");
    input.name="function";input.type="radio";
    input.onchange=()=>{canv.definingFunction=functions[func];if(update.auto)update(canv);};
    span.appendChild(input);
    field.appendChild(span);
  }
  {
   let kay=document.getElementById("K");
   kay.oninput=()=>{canv.K=kay.value;};
   document.getElementById("reset").onclick=()=>{lowerX=lowerY=-2;higherX=higherY=2;update(canv)};
   document.getElementById("updating").onclick=()=>{update(canv);};
  }
  return field;
}

function updateLHC(square){
  let spanX=higherX-lowerX;
  let spanY=higherY-lowerY;
  let lowValX=lowerX+(square.start.x/CANVSIZE)*spanX;
  let highValX=lowerX+((square.start.x+square.a)/CANVSIZE)*spanX;
  let lowValY=lowerY+(square.start.y/CANVSIZE)*spanY;
  let highValY=lowerY+((square.start.y+square.a)/CANVSIZE)*spanY;
  lowerX=lowValX;
  lowerY=lowValY;
  higherX=highValX;
  higherY=highValY;
}

function colorMethodCheck(canv){
  let field=document.createElement("fieldset");
  field.id="colorm";
  {
    let legend=document.createElement("legend");
    legend.textContent="Coloring method";
    field.appendChild(legend);
  }
  for(let method in coloring){
      let span=document.createElement("span");
      span.textContent="Method: "+method;
      //span.style.border="solid 2px black";
      span.style="border:solid 2px black;white-space:nowrap;float:left;";
      //span.style.whiteSpace="nowrap";
      //span.style.float="left";
      let input=document.createElement("input");
      input.name="method";input.type="radio";
      input.onchange=()=>{canv.coloringMethod=coloring[method];if(update.auto)update(canv);};
      span.appendChild(input);
      field.appendChild(span);
  }
  return field;
}

function check(complexNumber,canv) {
    if(canv.definingFunction.isReady)
      var f=canv.definingFunction.function;
    else
      var f=canv.definingFunction.construct(complexNumber,canv.K);
    if(canv.definingFunction.use!=null)
      var val=canv.definingFunction.use;
    else
      var val=complexNumber;
    if(update.precise)
      val=ComplexNumberDecimal.fromCN(val);
    let result={
      "i":0,
      "smallest":val,
      "smallInd":val,
      "final":val
    };
    let imaginary=99,real=99;
		while(result.i<255 && val.magnitude()<4) {
			val=f(val);
      if(val.magnitude()<result.smallest.magnitude())
        result.smallest=val;
      if(Math.abs(val.real)<real)
        real=Math.abs(val.real);
      if(Math.abs(val.imaginary)<imaginary)
        imaginary=Math.abs(val.imaginary);
      if(!val.isFinite()){
        result.final=val;
        result.smallInd=new ComplexNumber(real,imaginary);
  			return result;
      }
      result.i++;
		}
    result.smallInd=new ComplexNumber(real,imaginary);
    result.final=val;
		return result;
	}

async function update(canv){
  let mes=ShowableMessage("Rendering...");
  let sets = [document.getElementById("options"),document.getElementById("colorm"),document.getElementById("func")];
  mes.show();
  sets.forEach(set=>set.disabled="true");
  await sleep(1);
  canv.con.fillStyle="black";
  canv.con.fillRect(0,0,canv.width,canv.height);
  let step=(higherX-lowerX)/CANVSIZE;
  for(let i=lowerX,x=0;x<canv.width;i+=step,x++){
    for(let j=lowerY,y=0;y<canv.height;j+=step,y++){
      let n=check(new ComplexNumber(i,j),canv);
      canv.con.fillStyle=rgbString(n,canv);
      canv.con.fillRect(x,y,1,1);
    }
    mes.update(x/CANVSIZE);
    await sleep(1);

  }
  canv.saveCurrent();
  mes.hide();
  sets.forEach(set=>set.disabled=false);
}
update.auto=true;
update.precise=false;

function rgbString(n,canvas){
  return canvas.coloringMethod(n.i,n.smallest,n.smallInd,n.final);
}

function multiArray(size,dimension,initial){
  if(dimension==1){
    let result = [];
    for(let i=0;i<size;i++){
      result.push(initial);
    }
    return result;
  }
  let result=[];
  for(let i=0;i<size;i++)
    result.push(multiArray(size,dimension-1,initial));
    if(result.length!=size)
    throw new Exception("What?");
  return result;
}

function ShowableMessage(message){
  let ob={};
  ob.html=document.createElement("p");
  ob.html.appendChild(document.createTextNode(message));
  let prog=document.createElement("progress");
  prog.max=1; prog.value=0;
  ob.html.appendChild(prog);
  ob.show=function(){
    document.body.insertBefore(ob.html,document.body.firstChild);
  }
  ob.hide=function(){
    document.body.removeChild(ob.html);
  }
  ob.update=(nr)=>{
    prog.value=nr;
  };
  return ob;
}


//Code from nice people from stackoverflow
function  getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect(), // abs. size of element
      scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
      scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

  return {
    x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
    y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
  }
}
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function detectLeftButton(evt) {
    evt = evt || window.event;
    if ("buttons" in evt) {
        return evt.buttons == 1;
    }
    var button = evt.which || evt.button;
    return button == 1;
}

function storeOnPage() {
  const canvas = document.querySelector("canvas");
  canvas.toBlob(blob=>{
    var newdiv = document.createElement("div");
    newdiv.style.float="left";
    newdiv.innerHTML = `
      <div style="border: 1px solid black"> From ${new Date}</div>
      <img class="toset" alt="loading" />
      <button class="toload">Download</button>
      <button class="remove">Remove</button>
    `;
    document.body.appendChild(newdiv);
    var newImg = newdiv.querySelector(".toset"),
        toload = newdiv.querySelector(".toload"),
        remove = newdiv.querySelector(".remove"),
        url = URL.createObjectURL(blob);
    newImg.onload = () =>URL.revokeObjectURL(url);
    newImg.src = url;
    toload.onclick = ()=> saveAs(blob,new Date().toString()+".png");
    remove.onclick = ()=> newdiv.remove();
  });
}

async function autoGenerateNoise(){
  autoGenerateNoise.on = true;
  const canvas = document.querySelector("canvas");
  canvas.definingFunction = functions["Noise K"];
  while(autoGenerateNoise.on){
    const method = Object.keys(coloring).choose();
    canvas.coloringMethod = coloring[method];
    canvas.K = (canvas.K-0) + (Math.random()-0.5) * 5000;
    await update(canvas);
    let blob = await new Promise(resolve=>{
        canvas.toBlob(blob=>resolve(blob));
    });
    saveAs(blob,method+canvas.K+".png");
  }
}

Array.prototype.choose = function () {
  return this[Math.floor(Math.random() * this.length)]
}