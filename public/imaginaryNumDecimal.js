"use strict";
class ComplexNumberDecimal {
    constructor(real, imaginary) {
        this._real = (real instanceof Decimal) ? real : new Decimal(real);
        this._imaginary = (imaginary instanceof Decimal) ? imaginary : new Decimal(imaginary);
    }
    get real() {
        return this._real.toNumber();
    }
    get imaginary() {
        return this._imaginary.toNumber();
    }
    get r() {
        return Math.sqrt(this.magnitude());
    }
    get _r() {
        return this._magnitude().sqrt();
    }
    get theta() {
        return this._theta.toNumber();
    }
    get _theta() {
        return this._imaginary.div(this._real).atan();
    }
    toString() {
        return `(${this._real.toPrecision(5)}+${this._imaginary.toPrecision(5)}i)`;
    }
    add(o) {
        if (o instanceof ComplexNumberDecimal || o instanceof ComplexNumber)
            return new ComplexNumberDecimal(this._real.add(o._real), this._imaginary.add(o._imaginary));
        return this.add(ComplexNumberDecimal.from(o));
    }
    sub(o) {
        if (o instanceof ComplexNumberDecimal || o instanceof ComplexNumber)
            return new ComplexNumberDecimal(this._real.sub(o._real), this._imaginary.sub(o._imaginary));
        return this.sub(ComplexNumberDecimal.from(o));
    }
    neg() {
        return this.muli(ComplexNumberDecimal.NONE);
    }
    muli(o) {
        if (o instanceof ComplexNumberDecimal || o instanceof ComplexNumber)
            return new ComplexNumberDecimal(this._real.mul(o._real).sub(this._imaginary.mul(o._imaginary)), this._imaginary.mul(o._real).add(this._real.mul(o._imaginary)));
        return this.muli(ComplexNumberDecimal.from(o));
    }
    mult(o) {
        return this.muli(o);
    }
    //I know that this is not mathematically correct. Will fix it at some point
    sin() {
        return new ComplexNumberDecimal(this._real.sin(), this._imaginary.sin());
    }
    exp() {
        return new ComplexNumberDecimal(this._real.exp().add(this._imaginary.cos()), this._imaginary.sin());
    }
    inv() {
        return new ComplexNumberDecimal(this._real, this._imaginary.neg());
    }
    isFinite() {
        return true; //this._real.isFinite() && this._imaginary.isFinite();
    }
    divise(o) {
        if (o == 0)
            throw new Error("x/0 is not allowed");
        if (o instanceof ComplexNumberDecimal || o instanceof ComplexNumber) {
            let val = (o._magnitude || o.magnitude)();
            return new ComplexNumberDecimal(this._real.mul(o._real).add(this._imaginary.mul(o._imaginary)).div(val), this._imaginary.mul(o._real).sub(this._real.mul(o._imaginary)).div(val));
        }
        return this.divise(ComplexNumberDecimal.from(o));
    }
    pow(o) {
        if (o instanceof ComplexNumberDecimal || o instanceof ComplexNumber)
            return this.powComplex(o);
        if (o <= 0)
            return ComplexNumberDecimal.ONE;
        return this.muli(this.pow(o - 1));
    }
    powComplex(o) {
        let logpart = this._r.log();
        return (new ComplexNumberDecimal(logpart.mul(o._real), logpart.mul(o._imaginary)).add(new ComplexNumberDecimal(0, this._theta).muli(o))).exp();
    }
    negInfinity() {
        return !this.isFinite();
    }
    magnitude() {
        return this._magnitude().toNumber();
    }
    _magnitude() {
        return this._real.pow(2).add(this._imaginary.pow(2));
    }
    static fromCN(cn) {
        return new ComplexNumberDecimal(cn.real, cn.imaginary);
    }
    static from(num) {
        return new ComplexNumberDecimal(num, 0);
    }
    ;
}
ComplexNumberDecimal.ZERO = ComplexNumberDecimal.from(0);
ComplexNumberDecimal.ONE = ComplexNumberDecimal.from(1);
ComplexNumberDecimal.NONE = ComplexNumberDecimal.from(-1);
