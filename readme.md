# Mandelbrot Site

This is one of the first js/html projects I ever tried out. I found the algorithm to generate mandelbrots, and wanted to play around with it.

Following that, I extended the page to include a lot of other algorithms, and coloring schemes, many of which I just made up.  

Later on, I started rewriting some of the files in typescript, but pretty quickly stopped with this, since I wasn't planning on working on the project any more anyway. 

## Future
I don't plan on adding to this repository any more.  
This readme file is written many years after I wrote any of this, where I am going over some old projects, and make sure that everything is either not public, or somewhat presentable. 
By now I have actually rewritten parts of this in another project that isn't public at this point. I might update this readme once it does become public.
